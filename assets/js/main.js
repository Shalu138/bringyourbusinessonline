$(document).ready(function(){
    $(window).bind("scroll", function () {
      var banner_height = $('.main-section').height();
      var headerheigth = $('header').height();
    //   var windowWidth = $(window).width();
  
        if ($(window).scrollTop() > headerheigth && $(window).scrollTop() < banner_height) {
          $('header').addClass('slidable');
        }
        else if ($(window).scrollTop() > banner_height) {
          $('header').addClass('sticky');
        }
        else {
          $('header').removeClass('sticky').removeClass('slidable');
        }
        if ($(window).scrollTop() == 0) {
          $('header').removeClass('sticky').removeClass('slidable');
        }
    });
    if ($(window).scrollTop() == 0) {
      $('header').removeClass('sticky').removeClass('slidable');
    }
});